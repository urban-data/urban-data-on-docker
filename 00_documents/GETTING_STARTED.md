# IoT Protype Platform - Getting started.

This documentation is intended to give an first tutorial how to work with the IoT Prototyping Plattform of the Urban Techn Republic Project.

We do not want to document all used components again, so we reference the original documentation where we need this.

This documentation should help for the first steps in using the Platform.

The follwoing chapters are available:
<br>
1. [Creating a new User in the central Identity Management](getting_started/IDM_create_user.md)
2. [Exploring data with interactive Grafana Dashboards](getting_started/GRAFANA_first_steps.md)
3. [Getting API Access for a registered User](getting_started/API_get_access.md)
