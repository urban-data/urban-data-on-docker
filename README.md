# Urban Data Platform on Docker

This repository provides you an automated setup for an Urban Data Base Platform based on a container deployment on a docker engine.

We started the work on this repository in May 2020 with the idea in mind, that at least an Proof-of-concept ready fully automated base platform for Smart City Use Cases should exist as a starting point for cities to experiment with Smart City Use Cases without reinventing the wheel again and again. As a core component of the Platform we use the [FIWARE](https://fiware.org) Context Broker, the SensorThingsAPI Server [FROST](https://github.com/FraunhoferIOSB/FROST-Server) and other Standard Open Source Components.

This first published version ist the starting point for this idea and we are highly motivated to let this grow as an Open Source Platform for Urban Data.

The current Scope of the Platform contains the following components:

* Identity Management
* Reverse Proxy with fully automated SSL Support for all components
* API Management
* Context Management
* Data Flow Management
* Historical Data Management
* Data Visualization

All used components are open source software components with active communities. This repository provides on top of the components itself the needed end-to-end configurations to use these tools in the Smart City Domain.

If you are interested please find more detailed information in the following documents:

* [Install](00_documents/INSTALL.md)

## Contributing

Before raising a pull-request, please read our
[contributing guide](00_documents/CONTRIBUTING.md).

This project adheres to the [Contributor Covenant 1.4](http://contributor-covenant.org/version/1/4/).
 By participating, you are expected to uphold this code. Please report unacceptable
 behavior to any of the project's core team (see Authors).

## Authors

Smart City on Docker is a project of the [UNITY AG](https://unity.de) in cooperation with [Hypertegrity AG](https://hypertegrity.de)

The initial Creators are:

* Andreas Linneweber [Email](mailto:andreas.linneweber@unity.de) [LinkedIn](http://linkedin.com/in/andreas-linneweber-82346625)
* Markus Luckey [Email](mailto:markus.luckey@unity.de) [Twitter](https://twitter.com/markusluckey) [LinkedIn](https://www.linkedin.com/in/markus-luckey-69674417)


## Copyright and license

Copyright Andreas Linneweber, Markus Luckey and other contributors under [EUPL-1.2](LICENSE).

## Known Issues

Currently there are two bigger issues to solve:

1. Create default.yml values by an additional playbook
2. Freeze Version Numbers per Component
