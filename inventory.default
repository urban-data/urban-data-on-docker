#########################################
# Inventory for Smart City Platform Setup
#########################################

#########################################################################
# The "servers" groups contains the remote hosts
#########################################################################

[servers]
demo ansible_host=<IP>

##########################################################################
# Makes sure we use Python 3 (/usr/bin/python3) on remote hosts.
# [servers:vars] sets global values for all nodes in the "servers" group.
# This is necessary because by default Ansible will try to use
# /usr/bin/python, which is not available on newer Ubuntu versions.
#########################################################################

[servers:vars]
ansible_python_interpreter=/usr/bin/python3

####################################################
# External Data - please provide valid configuration
####################################################

# Subdomain for the PoC Installation
DOMAIN=<server.domain.com>
# Emails Address for Let's Encrypt Registration
EMAIL=dns@urban-data.cloud

# Unix User Name to run to Platform
CREATE_USER=mrdocker

# Filename of private ssh key to use - standard: id_rsa_sc_admin
SSH_KEY_NAME='id_rsa_sc_admin'

# Admin Password for installation
SC_ADMIN_PASSWORD='<password>'

# Sender Email Address for generated Emails
SC_EMAIL_FROM='demo@<server.domain.com>'
# Email SMTP Server
SC_EMAIL_SERVER='<email.domain.com>'
# Email Server Login
SC_EMAIL_USER='<user>'
# Email Server Password
SC_EMAIL_PASSWORD='<password>'

# API Key for Open Weather Map Access
SC_OWM_API_KEY='<OWN_API_KEY>'
# ID of wished OWM Station
SC_OWM_STATION='OWM_STATION_ID'
# ID of Wished Open Sense Map Sensor
SC_OSM_SENSOR_ID='<OSM_SENSOR_ID>'

# Gitlab User to clone
NR_GIT_LOGIN='<user>'
# Git Token or Password.
# Depending on your authentication method please provide you login credentials (e.g. if you use 2FA use a specific token and so on)
NR_GIT_TOKEN='<token>'
# Repo for internal data Flows (Broker to Timeseries)
# Example: gitlab.com/urban-data/demo-flows/urban-data-internal-demo-flows.git
NR_INT_GIT_REPO='gitlab.com/urban-data/demo-flows/urban-data-internal-demo-flows.git'
# Repo for external flows (external API Integration)
# Example: gitlab.com/urban-data/demo-flows/urban-data-external-demo-flows.git
NR_EXT_GIT_REPO='gitlab.com/urban-data/demo-flows/urban-data-external-demo-flows.git' 
# Repo for external flows (Connector Integration)
# Example: gitlab.com/urban-data/demo-flows/urban-data-conn-demo-flows.git
NR_CON_GIT_REPO='gitlab.com/urban-data/demo-flows/urban-data-conn-demo-flows.git' 
# Repo Target folder
NR_REPO_TARGET_FOLDER='sc-node-red-flows'
# Key to encrypt/decrypt Nodered Secrets in Node Red Project
NR_SECRETS_KEY='<nodered_secrets_key>'

# Open Weather Map API Key
SC_OWM_API_KEY='<OWM_API_KEY>'
# Open Weather Map Station ID
SC_OWM_STATION='<OWM_STATION_ID>'
# Open Sensor Map Sensor ID
SC_OSM_SENSOR_ID='<OSM_SENSOR_ID>'

# TimescaleDB Password
TIMESCALE_PASSWORD='<password>'

# FROST-DB Password
FROST_DB_PASSWORD='<password>'

# Key for Session Encryption of keycloak-gatekeyer (16 or 32 signs)
AUTH_ENCRYPTION_KEY='<AUTH_KEY>'
